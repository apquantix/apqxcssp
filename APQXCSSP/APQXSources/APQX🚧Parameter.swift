//
//  APQX🚧Parameter.swift
//  APQXCSSP
//
//  Created by Bonifatio Hartono on 4/4/16.
//  Copyright © 2016 Bonifatio Hartono. All rights reserved.
//

import Foundation

public class APQX🚧Parameter{
    public var parameter : Any?
    var paramDesc : String = ""
    
    public init(param: Any? , paramDesc: String){
        self.parameter = param
        self.paramDesc = paramDesc
    }
    
    public func load(inSolution: APQX🚧Solution){
        inSolution.parameters.append(self)
    }
}