//
//  APQX🚧Sequence.swift
//  APQXCSSP
//
//  Created by Bonifatio Hartono on 4/4/16.
//  Copyright © 2016 Bonifatio Hartono. All rights reserved.
//

import Foundation

/* Sequence can skip if there are no solution */
public class APQX🚧Sequence{
    var description = ""
    //var solution : Solution = Solution()
    public init(desc: String){
        self.description = desc
    }
    public func load(inX: APQX🚧App){
        inX.sequences.append(self)
    }
}
