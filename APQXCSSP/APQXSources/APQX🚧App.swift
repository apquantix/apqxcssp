//
//  APQX🚧App.swift
//  APQXCSSP
//
//  Created by Bonifatio Hartono on 4/4/16.
//  Copyright © 2016 Bonifatio Hartono. All rights reserved.
//

import Foundation

/* APQXApp, Cases, Sequences, Solution, Parameters */
public class APQX🚧App{
    
    /** for multi class test cases might need singleton */
    static let 🔮simpleApp = APQX🚧App()
    
    /** Ordered array of APQX cases */
    var cases : [APQX🚧Case] = []
    
    /** Ordered array of APQX sequences */
    var sequences : [APQX🚧Sequence] = []
    
    /** Dictionary, Key: APQX🚧Case.description, Value: case code (ex: C01) */
    var caseCode : [String: String] = [:]
    
    /** Dictionary, Key: case code (ex: C01), Value: APQX🚧Case.description */
    var caseDesc : [String: String] = [:]
    
    /** Dictionary, Key: APQX🚧Sequence.description, Value: sequence code (ex: S01) */
    var seqCode : [String: String] = [:]
    
    /** Dictionary, Key: sequence code (ex: S01), Value: APQX🚧Sequence.description */
    var seqDesc : [String: String] = [:]
    
    /** Ordered array of casseqs codes C01S01, C02S01,... */
    var casseqsIndexC : [String] = []
    
    /** Ordered array of casseqs code ... C01S01, C01S02, ... */
    var casseqsIndexS : [String] = []
    
    /** Dictionary, Key: case+sequence code (ex: C01S01), Value: APQX🚧Solution */
    var casseqs : [String:APQX🚧Solution] = [:]
    
    /** Dictionary, Key: casseqs code (ex: C01S01), Value: concatenated case desc and sequence desc */
    var casseqsDesc : [String: String] = [:]
    
    /** Dictionary, Key: concatenated case desc and sequence desc,  Value: casseqs code (ex: C01S01) */
    var casseqsCode : [String: String] = [:]
    
    /** Dictionary, Key: concatenated case desc and sequence desc,  Value: APQX🚧Solution */
    public var casseqsSolution : [String: APQX🚧Solution] = [:]
    
    public init(){
        
    }
    
    public func compileSequences(){
        self.casseqsIndexC = []
        self.casseqsIndexS = []
        
        _ = self.cases.enumerate().map { (caseIDX, inCase) in
            let caseIndex = String(format: "%02d", caseIDX+1)
            _ = self.sequences.enumerate().map({ (seqIDX, inSeq) in
                
                let seqIndex = String(format: "%02d", seqIDX+1)
                let cCode = "C\(caseIndex)" // CXX
                let sCode = "S\(seqIndex)" // SXX
                let casseqsCode = "\(cCode)\(sCode)" // CXXSXX
                
                self.casseqsIndexC.append(casseqsCode)
                self.casseqsDesc[casseqsCode] = "\(inCase.description)@\(inSeq.description)"
                self.casseqsCode["\(inCase.description)@\(inSeq.description)"] = casseqsCode
                
                self.caseCode[inCase.description] = cCode
                self.caseDesc[sCode] = inCase.description
                
                self.seqCode[inSeq.description] = sCode
                self.seqDesc[sCode] = inSeq.description
            })
        }
        _ = self.sequences.enumerate().map { (seqIDX, inSeq) in
            let seqIndex = String(format: "%02d", seqIDX+1)

            _ = self.cases.enumerate().map { (caseIDX, inCase) in
                let caseIndex = String(format: "%02d", caseIDX+1)
                let cCode = "C\(caseIndex)" // CXX
                let sCode = "S\(seqIndex)" // SXX
                let casseqsCode = "\(cCode)\(sCode)" // CXXSXX
                self.casseqsIndexS.append(casseqsCode)
                
            }
        }
    }
    
    public func compileSolution(inSolution : APQX🚧Solution) -> APQX🚧Solution{
        
        /* Re compile sequences? */
        self.compileSequences()
        
        // find it's case
        let aCase : APQX🚧Case? = self.cases.filter{ $0.description == inSolution.regCase }.first
        
        // find it's sequence
        let aSequence : APQX🚧Sequence? = self.sequences.filter{ $0.description == inSolution.regSequence }.first
        
        // adding case and seq to solution
        inSolution.sCase = aCase
        inSolution.sSeq = aSequence
        
        let aCaseDesc : String = aCase!.description
        let aSeqDesc : String = aSequence!.description
        
        
        let casseqsCode : String  = self.casseqsCode["\(aCaseDesc)@\(aSeqDesc)"]!
        self.casseqs[casseqsCode] = inSolution
        self.casseqsSolution["\(aCaseDesc)@\(aSeqDesc)"] = inSolution
        
        return inSolution
    }
    
    public func sequenceIntegrationC() {
        self.sequenceIntegration(self.casseqsIndexC)
    }
    public func sequenceIntegrationS() {
        self.sequenceIntegration(self.casseqsIndexS)
    }
    
    /** Prints signature when there is no Solution */
    var shouldPrintSignature = false
    
    /** Only first nil solution will be printed */
    var stopAtFirstSignature = false
    
    /** have you been here? */
    private var hasBeenHere = false
    
    
    private func sequenceIntegration(inArray : [String]){
        var nextNodes : [String] = []
        var emptySolutionCount = 0
        print("\n\n🚧🚧🚧🚧🚧")
        _ = inArray.map { (inString) in
            
            /* Guard against casseqs without Solution */
            guard self.casseqs[inString] != nil else{
                emptySolutionCount += 1
                /* skip if it has been here and if user ask to stop at first signature */
                if self.hasBeenHere && self.stopAtFirstSignature{
                    return
                }
                print("💃💃💃")
                self.reportFormat(inString)
                
                if self.shouldPrintSignature{
                    print("Next Node: test\(self.casseqsDesc[inString]!.functionalizedString())")
                    nextNodes.append(self.casseqsDesc[inString]!)
                }
                
                self.hasBeenHere = true
                return
            }
            print("💃💃💃")
            
            let aSolution = self.casseqs[inString]
            let params = aSolution!.parameters
            
            // iterate through params if any
            
            var paramString : NSString = ""
            _ = params.map({ (inParam) in
                paramString = "\(paramString), \(inParam.paramDesc)"
            })
            
            if paramString.length > 2 {
                paramString = paramString.substringFromIndex(2)
                self.reportFormat(inString)
            }
            else{
                self.reportFormat(inString)
            }
        }
        
        print("\n")
        let casesCount = self.cases.count
        let currentCount = emptySolutionCount % casesCount
        //let remainingSolutionInCurrentSequences = casesCount - remainder
        
        print("Remaining Empty Solution: \(emptySolutionCount)")
        print("Number of cases \(casesCount)")
        print("Solution left for this Sequences: \(currentCount)")
        _ = nextNodes.map{ print ("NextNode(s): " + $0 )}
        print("🚧🚧🚧🚧🚧\n\n")
    }
    
    func reportFormat(inString: String){
        let casseqsDesc = self.casseqsDesc[inString]!
        let aSolution : APQX🚧Solution? = self.casseqs[inString]

        print("Casseqs     : \(inString)")
        print("Description : \(casseqsDesc)")
        print("Solution    : \(aSolution)")
        
        /* If solution exists and has description */
        if let aSolutionDesc = aSolution?.description {
            
            print("Signature   : ¬ \n\t\(aSolutionDesc)")
            
            if aSolution?.parameters.count == 0 {
                print("Parameters   : -- NONE --")
                return
            }

            else if aSolution?.parameters.count > 5 {
                print("Parameters   : -- WARN Too many parameters to display --")
                return
            }
            else {
                print("Parameters  : ¬ ")
                /* If parameter exists */
                _ = aSolution?.parameters.map({ (cParam) in
                    print("\t\(cParam.paramDesc)")
                })
            }

            /* Results */
            let aRes = aSolution?.result
            print("Result      : ¬\n\((aRes.debugDescription))")
        }
        
        
        
    }
    
    @available(*, deprecated=1.0, renamed="sequenceIntegrationC")
    public func sequenceIntegration(){
        /* default using C base */
        self.sequenceIntegrationC()
    }
    
}


