//
//  APQX🚧Case.swift
//  APQXCSSP
//
//  Created by Bonifatio Hartono on 4/4/16.
//  Copyright © 2016 Bonifatio Hartono. All rights reserved.
//

import Foundation

public class APQX🚧Case{
    var description = ""
    public init(desc: String){
        self.description = desc
    }
    public func load(inX: APQX🚧App){
        inX.cases.append(self)
    }
}

