//
//  APQX🚧Solution.swift
//  APQXCSSP
//
//  Created by Bonifatio Hartono on 4/4/16.
//  Copyright © 2016 Bonifatio Hartono. All rights reserved.
//

import Foundation

/** Solution content is a formula, parameters, and result */
public class APQX🚧Solution{
    /* order matters */
    var parameters : [APQX🚧Parameter] = []
    public var result : Any?
    var regCase = ""
    
    var regSequence = ""
    var sCase: APQX🚧Case?
    var sSeq: APQX🚧Sequence?
    
    var description : String = ""
    
    public init(){
        
    }
    convenience public init(
        inCase: String,
        inSequence: String,
        inParams: [APQX🚧Parameter]){
        self.init()
        self.regCase = inCase
        self.regSequence = inSequence
        self.parameters = inParams
    }

    convenience init(inCase: String,
                     inSeqs: String){
        self.init(inCase: inCase, inSequence: inSeqs, inParams: [])
    }

    func loadCaseStr(inCase: String) -> APQX🚧Solution{
        self.regCase = inCase
        return self
    }
    func loadSeqStr(inSeq: String) -> APQX🚧Solution{
        self.regSequence = inSeq
        return self
    }
    func loadParams(inParam: [APQX🚧Parameter]) -> APQX🚧Solution {
        self.parameters = inParam
        return self
    }
    func addParam(inParam : APQX🚧Parameter) -> APQX🚧Solution {
        self.parameters.append(inParam)
        return self
    }
    
    public func formula(solution: (_:[APQX🚧Parameter]) -> Any? ) -> APQX🚧Solution{
        self.result = solution(self.parameters)
        return self
    }
    
    public func fastFormula(solution: () -> Any? ) -> APQX🚧Solution{
        self.result = solution()
        return self
    }
    public func load(inX : APQX🚧App, desc: String) -> APQX🚧Solution{
        self.description = desc
        return inX.compileSolution(self)
    }
}

