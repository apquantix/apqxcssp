//
//  ACSSP.swift
//  ACSSP
//
//  Created by Bonifatio Hartono on 4/3/16.
//  Copyright © 2016 Bonifatio Hartono. All rights reserved.
//

import Foundation

extension String{
    func functionalizedString() -> String{
        var str = self.capitalizedString
        str = str.stringByReplacingOccurrencesOfString("@", withString: "  ")
        str = str.stringByReplacingOccurrencesOfString("-", withString: "  ")
        str = str.stringByReplacingOccurrencesOfString(" ", withString: "")
        return str
    }
}
