//
//  APQXCSSPTests.swift
//  APQXCSSPTests
//
//  Created by Bonifatio Hartono on 4/3/16.
//  Copyright © 2016 Bonifatio Hartono. All rights reserved.
//

import XCTest
@testable import APQXCSSP

class APQXCSSPTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let X = APQX🚧App()
        /// Case Registry
        APQX🚧Case(desc: "Param Test").load(X)
        APQX🚧Case(desc: "Display 1").load(X)
        APQX🚧Case(desc: "Display Formula in Report").load(X)
        APQX🚧Case(desc: "Display Param in Report").load(X)
        APQX🚧Case(desc: "Display Result in Report").load(X)
        APQX🚧Case(desc: "Perform Chaining").load(X)
        
        APQX🚧Sequence(desc: "Test").load(X)
        APQX🚧Sequence(desc: "Seq 1").load(X)
        
        //X.compileSequences()
        
        
        
        
        /** Solution */
        let checkResult =
        APQX🚧Solution(inCase: "Display 1", inSequence: "Seq 1", inParams: []).formula { (inParam) -> Any in
            let x : Int32 = 5 + 5
            return NSNumber(int: x)
            }.load(X, desc: "NSNumber(int: 10)").result as! NSNumber
        XCTAssertEqual(checkResult, NSNumber(int: 10))
        
        
        /** Solution */
        let aParameters : [APQX🚧Parameter] = [APQX🚧Parameter(param: NSNumber(int: 3), paramDesc: "NSNumber(int: 3)")]
        APQX🚧Solution(inCase: "Param Test", inSequence: "Test", inParams: aParameters).formula { (inParam) -> Any in
            let param : APQX🚧Parameter = inParam.first!
            let aNumber : NSNumber = param.parameter as! NSNumber
            return NSNumber(int: aNumber.intValue)
            }.load(X, desc: "NSNumber(int: $0)")

        
        /** Solution */
        let param1 = APQX🚧Parameter(param: NSNumber(int: 3), paramDesc: "NSNumber(int: 3)")
        let param2 = APQX🚧Parameter(param: NSNumber(int: 3), paramDesc: "\"check\"")
        let aParametersArray : [APQX🚧Parameter] = [param1,param2]
        APQX🚧Solution(inCase: "Param Test", inSequence: "Test", inParams: aParametersArray).formula { (inParam) -> Any in
            let param : APQX🚧Parameter = inParam.first!
            let aNumber : NSNumber = param.parameter as! NSNumber
            return NSNumber(int: aNumber.intValue)
            }.load(X, desc: "NSNumber(int: $0)")
        
        
        /** Solution */
        APQX🚧Solution(inCase: "Display Result in Report", inSequence: "Test", inParams: []).formula { (inParam) -> Any in
            return ["test" : 0, "test2" : 1]
            }.load(X, desc: "[\"test\" : 0, \"test2\" : 1]")

        
        
        
        /* Chaining results */
        APQX🚧Solution(
            inCase: "Perform Chaining",
            inSequence: "Test",
            inParams: aParameters).formula { (inParam) -> NSString in
                return NSString(string: "Selvi Cupu")
            }.load(X, desc: "NSString(string: \"Selvi Cupu\")")
        
        let aResult =
        APQX🚧Solution(
            inCase: "Perform Chaining",
            inSequence: "Seq 1",
            inParams: aParameters).formula { (inParam) -> Any in
                let prevSolution : APQX🚧Solution = X.casseqsSolution["Perform Chaining@Test"]!
                let aResult : NSString = prevSolution.result as! NSString
                return aResult.stringByAppendingString(" Banget")
            }.load(X, desc: ".stringByAppendingString(\" Banget\")").result
        
        print(aResult)
        
        /* End of Solution */
        X.sequenceIntegrationC()
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
