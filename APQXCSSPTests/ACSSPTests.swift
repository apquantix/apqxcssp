//
//  ACSSPTests.swift
//  ACSSPTests
//
//  Created by Bonifatio Hartono on 4/3/16.
//  Copyright © 2016 Bonifatio Hartono. All rights reserved.
//

import XCTest
@testable import ACSSP

class ACSSPTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let X = Application()
        /// Case Registry
        Case(desc: "Display 0").load(X)
        Case(desc: "Display 1").load(X)
        Case(desc: "Display 2").load(X)
        Case(desc: "Display 3").load(X)
        
        Sequence(desc: "Seq 1").load(X)
        Sequence(desc: "Seq 2").load(X)
        Sequence(desc: "Seq 3").load(X)
        
        X.compileSequences()
        
        Solution(inCase: "Display 1", inSequence: "Seq 1").formula { (inParam) -> Any in
            let x : Int32 = 5 + 5
            return NSNumber(int: x)
            }.load(X)
        
        X.sequenceIntegration()

        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
